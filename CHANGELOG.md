<a name="0.2.0"></a>
# [0.2.0](https://bitbucket.org/atlassian/pact-json-schema/compare/0.1.0...0.2.0) (2017-10-06)


### Features

* add metadata definition to v1 and v2 spec ([898ee01](https://bitbucket.org/atlassian/pact-json-schema/commits/898ee01))



<a name="0.1.0"></a>
# [0.1.0](https://bitbucket.org/atlassian/pact-json-schema/compare/0.0.4...0.1.0) (2017-10-05)


### Features

* add schema for pact v2.0.0 ([823ee9e](https://bitbucket.org/atlassian/pact-json-schema/commits/823ee9e))


### BREAKING CHANGES

* The object returned when pact-json-schema is required in nodejs code has changed.

To migrate, change this:
const pactJsonSchemaV1 = require('pact-json-schema');

To this:
const pactJsonSchemaV1 = require('pact-json-schema').v1();



<a name="0.0.4"></a>
## [0.0.4](https://bitbucket.org/atlassian/pact-json-schema/compare/0.0.3...v0.0.4) (2017-08-09)


### Bug Fixes

* add request body to the schema ([6d84420](https://bitbucket.org/atlassian/pact-json-schema/commits/6d84420))



<a name="0.0.3"></a>
## [0.0.3](https://bitbucket.org/atlassian/pact-json-schema/compare/0.0.2...v0.0.3) (2017-03-28)


### Bug Fixes

* make interaction.response.status an integer ([b3b7cf9](https://bitbucket.org/atlassian/pact-json-schema/commits/b3b7cf9))
* update Pact version 1.x based on feedback from Pact community ([d2b4229](https://bitbucket.org/atlassian/pact-json-schema/commits/d2b4229)), closes [#4](https://bitbucket.org/atlassian/pact-json-schema/issue/4)


### Features

* add schema for Pact version 1.1.0 ([529795b](https://bitbucket.org/atlassian/pact-json-schema/commits/529795b))



<a name="0.0.2"></a>
## [0.0.2](https://bitbucket.org/atlassian/pact-json-schema/compare/0.0.1...v0.0.2) (2017-03-10)


### Features

* include only schemas in node module ([fea2ccb](https://bitbucket.org/atlassian/pact-json-schema/commits/fea2ccb))



<a name="0.0.1"></a>
# 0.0.1 (2017-03-10)


### Features

* add schema for Pact version 1.0.0 ([a07b477](https://bitbucket.org/atlassian/pact-json-schema/commits/a07b477))
