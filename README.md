# Pact JSON Schema
> A JSON Schema for Pact files.

## What is Pact JSON Schema
- A collection of JSON Schema files for each version of Pact
- Comprehensively tested using example Pact files

## Requirements
- npm 2.x or higher
- A JSON Schema v4 validator

## Installation

```
npm install pact-json-schema
```

## Usage

In NodeJS the JSON Schemas can be loaded using `require`

```
const pactJsonSchema = require('pact-json-schema');
 
const pactJsonSchemaV1 = pactJsonSchema.v1();
const pactJsonSchemaV2 = pactJsonSchema.v2();
```
 
For other languages the JSON Schemas can be found in the `schemas` folder.

Currently there are Schemas for the following Pact specification versions:

- 1.0.0 - `schemas/v1/schema.json`
- 1.1.0 - `schemas/v1/schema.json`
- 2.0.0 - `schemas/v2/schema.json`

## Changelog
See [CHANGELOG.md](CHANGELOG.md)

## Contributing
See [CONTRIBUTING.md](CONTRIBUTING.md)

## License
See [LICENSE.txt](LICENSE.txt)
