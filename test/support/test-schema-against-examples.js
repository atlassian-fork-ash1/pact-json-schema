'use strict';

const Ajv = require('ajv');
const jsonSchemaV4MetaSchema = require('ajv/lib/refs/json-schema-draft-04.json');
const fs = require('fs');
const VError = require('verror');

// eslint-disable-next-line no-sync
const forEachFileInDirectory = (directory, callback) => fs.readdirSync(directory).forEach(callback);

const loadJsonSync = (path) => {
    try {
        // eslint-disable-next-line no-sync
        return JSON.parse(fs.readFileSync(path));
    } catch (error) {
        throw new VError(error, `Unable to load json file "${path}"`);
    }
};

module.exports = (schemaPath, examplesPath) => {
    const schema = loadJsonSync(schemaPath);
    const ajv = new Ajv();
    ajv.addMetaSchema(jsonSchemaV4MetaSchema);
    const validate = ajv.compile(schema);

    const validateAgainstJsonSchema = (jsonPath) => {
        const json = loadJsonSync(jsonPath);
        const valid = validate(json);

        return {
            errors: validate.errors,
            isValid: valid
        };
    };

    forEachFileInDirectory(`${examplesPath}/pass`, (passExample) => {
        it(`should pass for "${examplesPath}/pass/${passExample}"`, () => {
            const result = validateAgainstJsonSchema(`${examplesPath}/pass/${passExample}`);

            expect(result.isValid).toBe(true, 'result.isValid');
            expect(result.errors).toBe(null, 'result.errors');
        });
    });

    forEachFileInDirectory(`${examplesPath}/fail`, (failExample) => {
        it(`should fail for "${failExample}"`, () => {
            const result = validateAgainstJsonSchema(`${examplesPath}/fail/${failExample}`);

            expect(result.isValid).toBe(false, 'result.isValid');
            expect(result.errors).not.toBe(null, 'result.errors');
        });
    });
};
