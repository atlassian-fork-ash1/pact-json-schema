'use strict';

module.exports = {
    v1: function v1 () {
        return require('./schemas/v1/schema.json');
    },
    v2: function v2 () {
        return require('./schemas/v2/schema.json');
    }
};
